package com.example.app

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.fragment_web.*

class MainActivity : AppCompatActivity() {
    var i = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme1);
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId){
            R.id.changeStyle->themeChanging()
        }
        return true
    }


    fun themeChanging(){
        if (i == 3) i = 0
        i++
        when(i){
            1 -> {
                setTheme(R.style.Theme1);
            }
            2 -> {
                setTheme(R.style.Theme2);
            }
            3 -> {
                setTheme(R.style.Theme3);
            }
        }
    }
    override fun onBackPressed() {
        if (webView.canGoBack()) webView.goBack() else super.onBackPressed()
    }
}